package controller;

import model.AirLineCompany;
import model.Airplane;
import model.BusinessLogic;

import java.util.ArrayList;
import java.util.List;

public class Controller {
    private BusinessLogic businessLogic;

    public Controller(){
        businessLogic = new BusinessLogic();
    }

    public int getMaxPassengers() {
        return businessLogic.getMaxPassengers();
    }

    public long getCargoVolume() {
        return businessLogic.getCargoVolume();
    }

    public List<Airplane> findByFuelCapacityBetween(Double from, Double to) {
        return businessLogic.findByFuelCapacityBetween(from, to);
    }

    public List<Airplane> getAirplanes() {
      return businessLogic.getAirplane();
    }


}

