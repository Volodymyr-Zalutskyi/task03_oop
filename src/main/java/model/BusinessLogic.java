package model;

import java.util.Iterator;
import java.util.List;

public class BusinessLogic {

    private AirLineCompany airLineCompany;

    public BusinessLogic(){
        airLineCompany = new AirLineCompany();
    }

    public int getMaxPassengers() {
        int count = 0;
        for (Airplane a : airLineCompany.getPlans()) {
            count += a.getPassengers();
        }
        return count;
    }

    public long getCargoVolume() {
        int count = 0;
        for (Airplane a : airLineCompany.getPlans()) {
            count += a.getCargoVolume();
        }
        return count;
    }

    public List<Airplane> findByFuelCapacityBetween(Double from, Double to) {
        Iterator<Airplane> iterator = airLineCompany.getPlans().iterator();
        while (iterator.hasNext()) {
            Airplane next = iterator.next();
            if (next.getFuelCapacity() >= from && next.getFuelCapacity() <= to) {
                airLineCompany.getPlans().add(next);
            }
        }
        return airLineCompany.getPlans();
    }

    public List<Airplane> getAirplane(){
        return airLineCompany.getPlans();
    }

}
