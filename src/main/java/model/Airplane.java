package model;

public class Airplane implements Comparable<Object> {

    private Manufacturer manufacturer;
    private long maxRange;
    private long fuelCapacity;
    private long cargoVolume;
    private int passengers;

    public enum Manufacturer {
        AIRBUS, BOEING, ANTONOV;
    }

    public Airplane(Manufacturer manufacturer, long maxRange, long fuelCapacity, long cargoVolume, int passengers) {
        this.manufacturer = manufacturer;
        this.maxRange = maxRange;
        this.fuelCapacity = fuelCapacity;
        this.cargoVolume = cargoVolume;
        this.passengers = passengers;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public long getMaxRange() {
        return maxRange;
    }

    public void setMaxRange(long maxRange) {
        this.maxRange = maxRange;
    }

    public long getFuelCapacity() {
        return fuelCapacity;
    }

    public void setFuelCapacity(long fuelCapacity) {
        this.fuelCapacity = fuelCapacity;
    }

    public long getCargoVolume() {
        return cargoVolume;
    }

    public void setCargoVolume(long cargoVolume) {
        this.cargoVolume = cargoVolume;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public int compareTo(Object o) {
        Airplane a = (Airplane) o;
        return (int) (a.fuelCapacity - this.fuelCapacity);
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "manufacturer=" + manufacturer +
                ", maxRange=" + maxRange +
                ", fuelCapacity=" + fuelCapacity +
                ", cargoVolume=" + cargoVolume +
                ", passengers=" + passengers +
                '}';
    }
}
