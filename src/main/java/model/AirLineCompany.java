package model;

import java.util.ArrayList;
import java.util.List;

public class AirLineCompany {

    private List<Airplane> plans;

    public AirLineCompany() {
        plans = new ArrayList<>();
        plans.add(new Airplane(Airplane.Manufacturer.AIRBUS, 15400, 320000, 175, 555));
        plans.add(new Airplane(Airplane.Manufacturer.BOEING, 15700, 138700, 137, 540));
        plans.add(new Airplane(Airplane.Manufacturer.ANTONOV, 15400, 300000, 1300, 6));

    }

    public void setPlans(List<Airplane> plans) {
        this.plans = plans;
    }

    public List<Airplane> getPlans() {
        return plans;
    }


}
