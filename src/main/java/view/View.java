package view;
import controller.Controller;
import java.util.*;
public class View {
    private Controller controller;
    Map<String, String> menu;
    Map<String, Printable> methodMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - Show all Airplane");
        menu.put("2", " 2 - Summary carrying capacity in all airplanes of your company");
        menu.put("3", " 3 - Summary passenger capacity in all airplanes of your company");
        menu.put("4", " 4 - Find airplanes with fuel consumption diapason");
        menu.put("0", " 0 - exit");

        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::pressButton1);
        methodMenu.put("2", this::pressButton2);
        methodMenu.put("3", this::pressButton3);
        methodMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        controller.getAirplanes().forEach(System.out::println);
    }

    private void pressButton2() {
        System.out.println("Summary carrying capacity: " + controller.getCargoVolume());
    }

    private void pressButton3() {
        System.out.println("Summary passenger capacity: " + controller.getMaxPassengers());
    }

    private void pressButton4() {
        System.out.println("Enter value of fuel consumption diapason");
        controller.findByFuelCapacityBetween(Double.valueOf(input.next()), Double.valueOf(input.next()))
                .forEach(System.out::println);
    }

    private void outPutMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outPutMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
