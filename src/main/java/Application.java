import controller.Controller;
import model.AirLineCompany;
import model.Airplane;
import model.BusinessLogic;
import view.View;

import java.util.Iterator;
import java.util.ListIterator;

public class Application {


    public static void main(String[] args) {
        new View().show();
    }


}
